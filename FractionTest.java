class FractionTest{
    public static void main(String[] args){
        Boolean[] test = {
            new Fraction(3).toString().equals("3"),
            new Fraction(-3).toString().equals("-3"),
            testIdentity(),
            new Fraction(3_000_000_000_000L).toString().equals("3000000000000"),
            new Fraction(1,3).toString().equals("1/3"),
            new Fraction(-1,3).toString().equals("-1/3"),
            new Fraction(1,-3).toString().equals("-1/3"),
            new Fraction(-1,-3).toString().equals("1/3"),
            new Fraction(7,3).toString().equals("2 1/3"),
            new Fraction(-7,3).toString().equals("-2 1/3"),
            new Fraction(7,-3).toString().equals("-2 1/3"),
            new Fraction(-7,-3).toString().equals("2 1/3"),
            new Fraction(3,4).add(new Fraction(1,4)).toString().equals("1"),
            new Fraction(3,4).add(new Fraction(0)).toString().equals("3/4"),
            new Fraction(0).add(new Fraction(3,4)).toString().equals("3/4"),
            new Fraction(0).add(new Fraction(0)).toString().equals("0"),
            new Fraction(3,4).add(new Fraction(1,4).neg()).toString().equals("1/2"),
            new Fraction(3,4).sub(new Fraction(1,4)).toString().equals("1/2"),
            new Fraction(6,4).add(new Fraction(5,3)).toString().equals("3 1/6"),
            new Fraction(6,4).add(new Fraction(-5,3)).toString().equals("-1/6"),
            new Fraction(-6,4).add(new Fraction(-5,3)).toString().equals("-3 1/6"),
            new Fraction(-6,4).add(new Fraction(5,3)).toString().equals("1/6"),
            new Fraction(3,4).mul(new Fraction(5,4)).toString().equals("15/16"),
            new Fraction(3,4).mul(new Fraction(5,4).inv()).toString().equals("3/5"),
            new Fraction(3,4).div(new Fraction(5,4)).toString().equals("3/5"),
        };
        
        int count=0;
        for(boolean i: test) if(!i) count++;
        for(int i=0; i<test.length; i++) if(!test[i]) System.out.println(i);

        System.out.printf("%d tests: discovered %d error(s)!\n", test.length, count);
        System.out.println(new Fraction(1,3));
    }

    static boolean testIdentity() {
        Fraction f = new Fraction(1/2);
        return f == f.add(new Fraction(0));
    }
}

class Fraction {
    public long zahler;
    public long nenner;

    public Fraction(long zahler, long nenner){
        this.zahler = zahler;
        this.nenner = nenner;
        simplify();
    }

    public Fraction(long zahler){
        this.zahler = zahler;
        this.nenner = 1L;
        simplify();
    }

    public static long gcd(long a, long b){
        if(b==0)
            return Math.abs(a);
        return Math.abs(gcd(b, a%b));
    }

    public long gcd(){
        return gcd(this.zahler, this.nenner);
    }

    public static long lcm(long a, long b){
        return Math.abs(a*b)/gcd(a,b);
    }

    public long lcm(){
        return lcm(this.zahler, this.nenner);
    }

    public Fraction neg(){
        this.zahler=-this.zahler;
        simplify();
        return this;
    }

    public Fraction inv(){
        long temp = this.zahler;
        this.zahler = this.nenner;
        this.nenner = temp;
        simplify();
        return this;
    }

    private void simplify(){
        this.zahler = zahler / gcd();
        this.nenner = nenner / gcd();
        
        if(this.nenner<0){
            this.zahler *= -1;
            this.nenner *= -1;
        }
    }

    public Fraction add(Fraction summand){
        this.zahler = this.zahler*summand.nenner + summand.zahler*this.nenner;
        this.nenner = this.nenner * summand.nenner;
        simplify();
        return this;
    }

    public Fraction sub(Fraction subtrahend){
        this.add(subtrahend.neg());
        simplify();
        return this;
    }

    public Fraction mul(Fraction factor){
        this.zahler = this.zahler * factor.zahler;
        this.nenner = this.nenner * factor.nenner;
        simplify();
        return this;
    }

    public Fraction div(Fraction factor){
        this.mul(factor.inv());
        simplify();
        return this;
    }

    public String toString(){
        simplify();
        String res="";
        if(this.zahler<0){
            res += "-";
        }

        long ganze = Math.abs(Math.abs(this.zahler)/Math.abs(this.nenner));
        long zahler = Math.abs(Math.abs(this.zahler)%Math.abs(this.nenner));
        long nenner = Math.abs(this.nenner);
        
        if(ganze!=0){
            res+=ganze;
        }

        if(zahler != 0 && nenner !=1){
            if(ganze!=0) res+=" ";
            res += zahler + "/" + nenner;
        }

        return res;
    }
}
